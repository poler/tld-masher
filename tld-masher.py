import sys
from nltk.corpus import wordnet

def load_tlds():
    return [word.strip() for word in open("tlds.txt", 'r')]

def find_domains(seed_words):
    tlds = load_tlds()

    domains = []
    for seed in seed_words:
        syns = wordnet.synsets(seed)
        all_syns = [hyp for syn in syns for hyp in syn.hypernyms()] + \
                   [hyp for syn in syns for hyp in syn.instance_hypernyms()] + \
                   [hyp for syn in syns for hyp in syn.hyponyms()] + \
                   [hyp for syn in syns for hyp in syn.instance_hyponyms()] + \
                   [hol for syn in syns for hol in syn.member_holonyms()] + \
                   [hol for syn in syns for hol in syn.substance_holonyms()] + \
                   [hol for syn in syns for hol in syn.part_holonyms()] + \
                   [mer for syn in syns for mer in syn.member_meronyms()] + \
                   [mer for syn in syns for mer in syn.substance_meronyms()] + \
                   [mer for syn in syns for mer in syn.part_meronyms()]
        lemmas = [lemma.name().lower().replace('_', '-') for syn in all_syns for lemma in syn.lemmas()]
        for word in set(lemmas):
            for tld in tlds:
                if word.endswith(tld):
                    domains.append("{}.{}".format(word[:-len(tld)].rstrip('-'), tld))

    return domains

if __name__ == '__main__':
    for domain in find_domains(sys.argv[1:]):
        print(domain)
