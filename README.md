For setup:

```
pip install nltk
python
>>> import nltk
>>> nltk.download('wordnet')
```

Usage:

```
$ ./find-available.sh fun
clowni.ng
punni.ng
```
