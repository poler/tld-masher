#!/bin/bash

for domain in $(python tld-masher.py $@);
do
    nslookup "$domain" 2>/dev/null 1>/dev/null;
    nslookup_result="$?";
    whois "$domain" 2>/dev/null | grep -qiE "no object found|not found";
    whois_result="$?";
    if [[ $nslookup_result -eq 1 && $whois_result -eq 0 ]];
    then
       echo "$domain";
    fi
done
